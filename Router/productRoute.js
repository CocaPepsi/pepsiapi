var express = require('express'),
    multer = require('multer'),
    path = require('path'),fs = require('fs-extra'), randomstring = require("randomstring");
var router = express.Router();
var productController = require('../Controller/productController.js');
var app = express();

localStorage.setItem("urlProduct",randomstring.generate(12));
var count = 0;
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        if(count===0 || localStorage.getItem("count")==="zo"){
            localStorage.setItem("urlProduct",randomstring.generate(12));
            count=1;
            localStorage.setItem("count","khongzo");
        }
        var getPath = `public/imgs/${localStorage.getItem("urlProduct")}/`;
        fs.mkdirsSync(getPath);
        cb(null, getPath);
    },  
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});

var upload = multer({
    storage: storage
});


/*
 * GET
 */
router.get('/', productController.list);
router.get('/topPrice', productController.LoadTop5Price);
router.get('/topAuc', productController.LoadTop5_countAuc);
router.get('/topTimeEnd', productController.LoadTop5_timeEnd);

/*
 * GET
 */
router.get('/:id', productController.show);

router.get('/search/detail', productController.search);

/*
 * POST
 */
router.post('/', productController.create);
router.post('/:id/Kich', productController.Kich);
<<<<<<< HEAD
router.post('/api/upload',upload.array('file',10), productController.upload);
=======
router.post('/api/upload',upload.array('file',3), productController.upload);
>>>>>>> deverlopment

/*
 * PUT
 */
router.put('/:id', productController.update);
router.put('/:id/AucPro', productController.AucPro);
router.put('/:id/Kich', productController.Kich);
/*
 * DELETE
 */
router.delete('/:id', productController.remove);

/*
 * DELETE
 */
router.post('/api/upload', productController.remove);

module.exports = router;

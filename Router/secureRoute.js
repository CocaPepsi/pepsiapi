var express = require('express');
var router = express.Router();
var userController = require('../Controller/userController.js');
var jwt = require('jsonwebtoken');

var SECRET_KEY = 'secret';

var fnCheckToken = (req, res, next) => {
    var token = req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, SECRET_KEY, (err, payload) => {
            if (err) {
                res.statusCode = 401;
                res.json({
                    msg: 'verify failed',
                    error: err
                });
            } else {
                req.tokenPayload = payload;
                next();
            }
        });
    } else {
        res.statusCode = 403;
        res.json({
            msg: 'no token found'
        });
    }
};

/*
 * GET
 */
router.get('/',fnCheckToken,userController.secured);

module.exports = router;

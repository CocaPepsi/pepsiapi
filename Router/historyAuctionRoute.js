var express = require('express');
var router = express.Router();
var historyAuctionController = require('../Controller/hisrotyAuctionController.js');

/*
 * GET
 */
router.get('/', historyAuctionController.list);

/*
 * GET
 */
router.get('/:id', historyAuctionController.show);

/*
 * POST
 */
router.post('/', historyAuctionController.create);

/*
 * PUT
 */
//router.put('/:id', historyAuctionController.update);

/*
 * DELETE
 */
router.delete('/:id', historyAuctionController.remove);

module.exports = router;

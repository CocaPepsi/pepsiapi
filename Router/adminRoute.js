var express = require('express');
var router = express.Router();
var adminController = require('../Controller/adminController.js');
var loginAdminController = require('../Controller/Auth/loginAdminController.js');
/*
 * GET
 */
router.get('/', adminController.list);

/*
 * GET
 */
router.get('/:id', adminController.show);
router.get('/:id/LoadLstUsReq', adminController.LoadLstUsReq);
router.get('/:id/CheckReqOfUs', adminController.CheckReqOfUs);

/*
 * POST
 */
router.post('/', adminController.create);
router.post('/login', loginAdminController.login);
/*
 * PUT
 */
router.put('/:id', adminController.update);
router.put('/:id/ResetPass', adminController.ResetPass);

/*
 * DELETE
 */
router.delete('/:id', adminController.remove);

module.exports = router;

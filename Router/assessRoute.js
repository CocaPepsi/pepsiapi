var express = require('express');
var router = express.Router();
var assessController = require('../Controller/assessController.js');

/*
 * GET
 */
router.get('/', assessController.list);

/*
 * GET
 */
router.get('/:id', assessController.show);

/*
 * POST
 */
router.post('/', assessController.create);

/*
 * PUT
 */
router.put('/:id', assessController.update);

/*
 * DELETE
 */
router.delete('/:id', assessController.remove);

module.exports = router;

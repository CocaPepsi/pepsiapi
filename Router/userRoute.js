var express = require('express');
var router = express.Router();
var userController = require('../Controller/userController.js');
var loginController = require('../Controller/Auth/loginController.js');

/*
 * GET
 */
router.get('/', userController.list);

/*
 * GET
 */
router.get('/:id', userController.show);
router.get('/:id/ListProBeingAucByUs', userController.ListProBeingAucByUs);
router.get('/:id/ListProDeposit', userController.ListProDeposit);
router.get('/:id/viewWatchList', userController.viewWatchList);
router.get('/:id/ListProAvaiByUs', userController.ListProAvaiByUs);
router.get('/:id/addWatchList', userController.addWatchList);
router.get('/:id/Requirement', userController.Requirement);
router.get('/:id/lstAucti', userController.lstAucti);


router.get('/:id/Requirement', userController.Requirement);
router.get('/:id/UpdateVip', userController.UpdateCatUserByAdmin);
/*
 * POST
 */
router.post('/', userController.create);
router.post('/:id/addWatchList', userController.addWatchList);
/*
 * PUT
 */
router.put('/:id', userController.update);
router.put('/:id/addLstAucWin', userController.addLstAucWin);


/*
 * DELETE
 */
router.delete('/:id', userController.remove);

/*
 * CAPTCHA
 */
router.post('/captcha', userController.captcha);

router.post('/login', loginController.login);


module.exports = router;



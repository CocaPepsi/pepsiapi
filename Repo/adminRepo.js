var adminModule = require('../Modules/adminModule.js');
var userModule = require('../Modules/userModule.js');

exports.LoadLstUsReq = function(req, res){
    userModule.find({'requirement' : true}, function(err, us) {
        if (err) {
            return res.status(500).json({
                message: 'Error when getting us.',
                error: err
            });
        }
            
        return res.json(us);
    });
}


//duyệt yc mún bán của US
exports.CheckReqOfUs = function(idUS){
    var now = new Date();
    userModule.findOne({_id: idUS}, function (err, user) {
        if (err) {
            return err;
        }
        if (!user) {
            return 'No such user';
        }

        user.requirement = false;
        user.catUser = true;
        user.dateGrant = now;
        user.dateExpired = new Date();
        user.dateExpired.setDate(now.getDate()+7);

        user.save(function (err, user) {
            if (err) {
                return err;
            }
            return true;
        });
       
        
    });
}

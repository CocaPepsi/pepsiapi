var userModule = require('../Modules/userModule.js');
var productModule = require('../Modules/productModule.js');


//cập nhật catUser khi hết hạn
exports.UpdateFieldCatUser = function(){
    console.log("asdas");
    var now = new Date();
    userModule.find({'dateExpired': {$lt : now}}, function (err, users) {
        if (err) {
            return err;
        }
        if (!users) {
            for (i in users){
                console.log("asdasd");
                userModule.findOne({_id: users[i].idUS}, function (err, user) {

                    //console.log(user);
                    if (err) {
                        return err;
                    }

                    user.requirement = false;
                    // Quick version
                    user.save(function (err, user) {
                        if (err) {
                            return err;
                        }
                        return true;
                    });
                });

                user.dateExpired = null;
                user.dateGrant = null;
                user.catUser = false;

                // Quick version
                user.save(function (err, user) {
                    if (err) {
                        return err;
                    }
                    
                });
            }

            return users;
        }
        return users;
    });
}

//thêm pro vào ds iu thích
exports.addWatchList = function(idUS, idPro){
    
    userModule.findOne({_id : idUS}, function (err, user) {
        if (err) {
            console.log(idUS);
            return err;
        }

        user.watchList.push(idPro);
        user.save(function (err, user) {
            if (err) {
                return err;
            }
            
        });
    });
}
//thêm pro vào ds đã thắng
exports.addLstAucWin = function(idUS, idPro, res){

    userModule.findOne({_id : idUS}, function (err, user) {
        if (err) {
            console.log(idUS);
            return err;
        }

        user.watchList.push(idPro);
        user.save(function (err, user) {
            if (err) {
                return err;
            }
            return res.json({user});
        });
    });
}

//xem ds pro iu thích
exports.viewWatchList = function(req, res){
    userModule.findOne({_id : req.params.id}, function (err, user) {
        if (err) {
            return err;
        }
        productModule.findOne({_id: {$in : user.watchList}}, function (err, products) {
            if (err) {
                return err;
            }
            return res.json({products});
        });
        
    });
}

//xem ds pro đang đấu giá
exports.lstAucti = function(req, res){
    userModule.findOne({_id : req.params.id}, function (err, user) {
        if (err) {
            return err;
        }
        productModule.findOne({_id: {$in : user.lstAucti}}, function (err, products) {
            if (err) {
                return err;
            }
            return res.json({products});
        });
        
    });
}

var express = require('express'),
    multer = require('multer'),
    path = require('path');
var app = express();

exports.uploads = ()=>{
    var staticDir = express.static(
        path.resolve(__dirname, 'public')
    );
    app.use(staticDir);
    var getPath = localStorage.getItem("urlProduct");
    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, getPath);
        },  
        filename: function(req, file, cb) {
            cb(null, file.originalname)
        }
    
    });
    
    var upload = multer({
        storage: storage
    });

    return upload;
}

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var productSchema = new Schema({
	'proName' : String,
	'price' : Number,//zá khởi đỉm
	'updatePriceAuction' : Number,//zá khách hàng đấu giá
	'pricebuyNow' : Number,//zá mua ngay
	//'priceMax' : Number,//za đc ra zá cao nhất hiện tại
	'jump' : Number,//bc nhảy
	'lstImg' : [],//da ảnh
	'timeAuc' : Number,//t.g đấu giá
	'postTime' : Date,//t.g đăng
	'timeEnd' : Date,//t.g kết thúc đấu zá
	'idSeller' : String,//id ngừi đăng
	'idUserMaxPrice' : String,
	'lstCateID' : [],
	'describe' : [],//mô tả
	'countAuc' : Number,
	'proNew' : Boolean,
	'autoIncreaseTime' : Boolean,//tự động tăng timeEnd (default false)
	'sold' : Boolean,//tình trạng đã bán hay chưa(true đã bán)
	'lstIdUserAuc' : [],//chứa ds các us đang đấu giá
	'lstIdUserKick' : [],//chứa ds các us bị kich khỏi pro đang đấu giá
	'top2' : [],
	'strFile':String
});

module.exports = mongoose.model('product', productSchema);

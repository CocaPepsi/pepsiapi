var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var userSchema = new Schema({
	'firstName' : String,
	'lastName' : String,
	'address' : String,
	'pass' : String,
	'eMail' : String,
	'requirement' : Boolean,//bật true khi xin yc đc bán và đang đợi duyệt
	'catUser' : Boolean,
	'assess' : Number,
	'dateGrant' : Date,//ngày duyệt yc
	'dateExpired' : Date,//ngày hết hạn
	'lstAucti' : [],//chứa ds các id của pro đang đấu giá
	'lstAucWin' : [],//chứa ds các id của pro đã thắng
	'lstAssess' : [],//chứa ds các Id đánh giá
	'watchList'	: []//chứa ds các id của pro iu thích
});

module.exports = mongoose.model('users', userSchema);

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var adminSchema = new Schema({
	'lstIdUsAllow' : [],//ngừi bị đánh zá
	'eMailAdmin' : String,//ngừi đáng zá
	'pass' : String,//lời nhắn
	'level' : Number//phân quyền cho admin
});

module.exports = mongoose.model('admin', adminSchema);

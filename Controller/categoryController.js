var categoryModule = require('../Modules/categoryModule.js');

/**
 * categoryController.js
 *
 * @description :: Server-side logic for managing cates.
 */
module.exports = {

    /**
     * categoryController.list()
     */
    list: function (req, res) {
        categoryModule.find(function (err, cate) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting cate.',
                    error: err
                });
            }
            return res.json(cate);
        });
    },

    /**
     * categoryController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        categoryModule.findOne({_id: id}, function (err, cate) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting cate.',
                    error: err
                });
            }
            if (!cate) {
                return res.status(404).json({
                    message: 'No such cate'
                });
            }
            return res.json(cate);
        });
    },

    /**
     * categoryController.create()
     */
    create: function (req, res) {
        var cate = new categoryModule({
            catName : req.body.catName,
            lstProduct : []

        });

        cate.save(function (err, cate) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating cate',
                    error: err
                });
            }
            return res.status(201).json(cate);
        });
    },

    /**
     * categoryController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        categoryModule.findOne({_id: id}, function (err, cate) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting cate',
                    error: err
                });
            }
            if (!cate) {
                return res.status(404).json({
                    message: 'No such cate'
                });
            }

            cate.catName = req.body.catName ? req.body.catName : cate.catName;
			cate.lstProduct = req.body.lstProduct ? req.body.lstProduct : cate.lstProduct;
           
            cate.save(function (err, cate) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating cate.',
                        error: err
                    });
                }

                return res.json(cate);
            });
        });
    },

    /**
     * categoryController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        categoryModule.findByIdAndRemove(id, function (err, cate) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the cate.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};

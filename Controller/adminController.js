var adminModule = require('../Modules/adminModule.js');
var adminRepo = require('../Repo/adminRepo.js');
var userModel = require('../Modules/userModule.js');
const bcrypt = require('bcrypt');

/**
 * adminController.js
 *
 * @description :: Server-side logic for managing ads.
 */
module.exports = {

    /**
     * adminController.list()
     */
    list: function (req, res) {
        adminModule.find(function (err, ad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ad.',
                    error: err
                });
            }
            return res.json(ad);
        });
    },

    /**
     * adminController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        adminModule.findOne({_id: id}, function (err, ad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ad.',
                    error: err
                });
            }
            if (!ad) {
                return res.status(404).json({
                    message: 'No such ad'
                });
            }
            return res.json(ad);
        });
    },

    /**
     * adminController.create()
     */
    create: function (req, res) {
        var ad = new adminModule({       
            eMailAdmin : req.body.eMailAdmin,
            pass : req.body.pass,
            level : 1,
            lstIdUsAllow : []
        });

        var hash = bcrypt.hashSync(ad.pass, 8);
        ad.pass = hash;

        ad.save(function (err, ad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating ad',
                    error: err
                });
            }
            return res.status(201).json(ad);
        });
    },

    /**
     * adminController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        adminModule.findOne({_id: id}, function (err, ad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ad',
                    error: err
                });
            }
            if (!ad) {
                return res.status(404).json({
                    message: 'No such ad'
                });
            }

            ad.eMailAdmin = req.body.eMailAdmin ? req.body.eMailAdmin : ad.eMailAdmin;
			
           
            ad.save(function (err, ad) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating ad.',
                        error: err
                    });
                }

                return res.json(ad);
            });
        });
    },

    /**
     * adminController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        adminModule.findByIdAndRemove(id, function (err, ad) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the ad.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    LoadLstUsReq: function (req, res) {
        return adminRepo.LoadLstUsReq(req, res);
    },

    CheckReqOfUs: function (req, res) {
        return adminRepo.CheckReqOfUs(req, res);
    },

    ResetPass: function (req, res) {
        var newPass = req.body.newPass;
        userModel.findOne({eMail : req.body.eMail}, function(err, us){
            if(err){
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }

            var hash = bcrypt.hashSync(newPass, 8);
            us.pass = hash;
            
            us.save(function (err, us) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating us.',
                        error: err
                    });
                }

                return res.json(us);
            });
        });
    }
};

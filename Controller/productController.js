var productModule = require('../Modules/productModule.js');
var categoryModule = require('../Modules/categoryModule.js');
var userModule = require('../Modules/userModule.js');
var productRepo = require('../Repo/productRepo.js');

var express = require('express'),
    mongoose = require('mongoose'),
    fs = require('fs-extra');

var app = express();
if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }
/**
 * productController.js
 *
 * @description :: Server-side logic for managing products.
 */
module.exports = {

    /**
     * productController.list()
     */
    list: function (req, res) {
        var page = req.query.page;
        return productRepo.LoadAll(req, res, +page);
    },

    /**
     * productController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        productModule.findOne({_id: id}, function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting product.',
                    error: err
                });
            }
            if (!product) {
                return res.status(404).json({
                    message: 'No such product'
                });
            }
            return res.json(product);
        });
    },
    

    /**
     * productController.create()
     */
    create: function (req, res) {
        var strFile = localStorage.getItem("urlProduct");
        var product = new productModule({
            proName : req.body.proName,
            price : req.body.price,
            pricebuyNow : req.body.pricebuyNow,
            jump : req.body.jump,
            lstImg : req.body.lstImg,
            lstCateID: req.body.lstCateID,
            autoIncreaseTime: req.body.autoIncreaseTime,
            updatePriceAuction : 0,
            countAuc: 0,
            strFile:strFile,
            idSeller : req.body.idSeller
        });

        product.postTime = Date.now();
        product.postTime.setHours(product.postTime.getHours() + 7);
        product.timeEnd = new Date();
        product.timeEnd.setHours(product.postTime.getHours() + req.body.timeAuc);

        var info_describe = req.body.describe;
        var time_describe = product.postTime;
        product.describe.push({info_describe, time_describe});

        product.proNew = true;          

        product.save(function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating product',
                    error: err
                });
            }
        });

        categoryModule.find({_id: {$in : product.lstCateID}}, function(err, cates){
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting cates.',
                    error: err
                });
            }
            for(var i=0;i<req.body.lstCateID.length;i++){
                var idCate = req.body.lstCateID[i];
                categoryModule.findOne({_id: idCate}, function(err, cate){
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when getting cate.',
                            error: err
                        });
                    }

                    cate.lstProduct.push(product._id);
                    cate.save(function(err, cate){
                        if (err) {
                            return res.status(500).json({
                                message: 'Error when getting cate.',
                                error: err
                            });
                        }
                    });
                });
            }
        });
        localStorage.setItem("count","zo");
        return res.status(201).json(product);
    },

    upload: function(req,res){        
        return res.json({msg:"dung roi"});
    },

    /**
     * productController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        var info_describe = req.body.describe;
        var time_describe = new Date();
        productModule.findOne({_id: id}, function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting product',
                    error: err
                });
            }
            if (!product) {
                return res.status(404).json({
                    message: 'No such '
                });
            }

            product.describe.push({info_describe, time_describe})

            /*product.proName = req.body.proName ? req.body.proName : product.proName;
			product.price = req.body.price ? req.body.price : product.price;
            product.pricebuyNow = req.body.pricebuyNow ? req.body.pricebuyNow : product.pricebuyNow;*/
            
            //product.assessSeller = req.body.assessSeller ? req.body.assessSeller : product.assessSeller;
            //product.idUserMaxPrice = req.body.idUserMaxPrice ? req.body.idUserMaxPrice : product.idUserMaxPrice;
            //product.assessUser = req.body.assessUser ? req.body.assessUser : product.assessUser;

			product.lstCateID = req.body.lstCateID ? req.body.lstCateID : product.lstCateID;
			
            product.save(function (err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating product.',
                        error: err
                    });
                }

                return res.json(product);
            });
        });
    },

    /**
     * productController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        productModule.findByIdAndRemove(id, function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the product.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    search: function (req, res) {
        var page = req.query.page;
        return productRepo.Search(req, res,+page);
    },

    LoadTop5Price: function (req, res) {
        return productRepo.LoadTop5Price(req, res);
    },

    LoadTop5_countAuc: function (req, res) {
        return productRepo.LoadTop5_countAuc(req, res);
    },

    LoadTop5_timeEnd: function (req, res) {
        return productRepo.LoadTop5_timeEnd(req, res);
    },

    sortByProperties : function (properties) {
        return function (x, y) {
            return ((x.PriceMax === y.PriceMax) ? 0 : ((x.PriceMax < y.PriceMax) ? 1 : -1));
        };
    },

    AucPro: function (req, res) {
        var idPro = req.params.id;

        var PriceMax = req.body.PriceMax;
        var idUS = req.body.idUS;

        var now = new Date();
        productModule.findOne({_id: idPro}, function (err, pro) {
            if (err) {
                return json({
                    message: 'Error when getting product',
                    error: err
                });
            }
            if (!pro) {
                return res.json({
                    message: 'No such '
                });
            }
            //console.log(pro.lstIdUserKick.indexOf(idUS));
            if(pro.lstIdUserKick.indexOf(idUS)>=0){
                return res.json({
                    message: 'you have been kich'
                });
            }
            else{
                pro.idUserMaxPrice = idUS;
                //push vào ds
                pro.lstIdUserAuc.push({idUS, "price" : pro.price});
                pro.price += pro.jump;
                pro.countAuc++;
                
                //sort tăng dần theo zá đấu zá
               /* var lst = pro.lstIdUserAuc;
                lst.sort(productRepo.sortByProperties('PriceMax'));*/
                //pro.updatePriceAuction = req.body.updatePriceAuction ? req.body.updatePriceAuction : pro.updatePriceAuction;
                

                if(PriceMax>0){
                    if(PriceMax >= +pro.pricebuyNow){
                        pro.lstIdUserAuc.push({idUS, "price" : PriceMax});
                        pro.idUserMaxPrice = idUS;
                        pro.proNew = null;
                        timeEnd = null;
                    }
                    else if(!pro.top2[0]){
                        pro.top2.push({idUS, PriceMax});
                        //pro.idUserMaxPrice = idUS;
                    }
                    else if(!pro.top2[1]){
                        pro.top2.push({idUS, PriceMax});
                        /*if(+pro.top2[1].PriceMax > +pro.top2[0].PriceMax){
                            pro.idUserMaxPrice = idUS;
                            pro.price = (+pro.top2[0].PriceMax);
                            pro.lstIdUserAuc.push({idUS, "Price" : pro.price});
                            //goi mail cho 0//bạn hien đang top 2
                        }*/
                        //sort zam theo PriceMax
                        pro.top2.sort(productRepo.sortByProperties('PriceMax'));
                    }
                    else if(PriceMax > +pro.top2[0].PriceMax){
                        //goi mail cho 0//da rot top

                        pro.top2.pop();
                        pro.top2.push({idUS, PriceMax});
                        pro.top2.sort(productRepo.sortByProperties('PriceMax'));
                        
                    }
                    else if(PriceMax > +pro.top2[1].PriceMax){
                        pro.top2.pop();
                        pro.top2.push({idUS, PriceMax});
                        //goi mail cho 1//da co ngui tra za hon
                       
                    }
                    //goi mail cho 1//da rot top
                    
                }


                //tự động đấu zá
                if(pro.top2.length == 2){
                    pro.idUserMaxPrice = pro.top2[0].idUS;
                    pro.price = pro.top2[1].PriceMax;
                    pro.countAuc++;
                    pro.lstIdUserAuc.push({"idUS" : pro.top2[0].idUS, "price" : pro.price});
                    pro.price += pro.jump;
                }
                else if(pro.top2.length > 0 && pro.top2[0].idUS != idUS ){
                    //tự động đấu zá cho top 1
                    pro.idUserMaxPrice = pro.top2[0].idUS;
                    pro.countAuc++;
                    pro.price = ((pro.price + pro.jump) > +pro.top2[0].PriceMax)? +pro.top2[0].PriceMax : pro.price + pro.jump;
                    pro.lstIdUserAuc.push({"idUS" : pro.top2[0].idUS, "price" : pro.price});
                    pro.price += pro.jump;
                }

                //truong hop za hien tai > za max cuar top2
                if(pro.top2.length > 0){
                    if(pro.price >= +pro.top2[0].PriceMax){
                        pro.top2 = [];
                        //goi mail cho ca 2//za hien tai đang > zá max
                    }
                    else if(pro.top2[1] && pro.price >= +pro.top2[1].PriceMax){
                        pro.top2.pop();
                        //goi mail cho 1//zá hiện tại đã lớn hơn
                    }
                }

                if( pro.autoIncreaseTime == true
                    && now.getDate() == pro.timeEnd.getDate() 
                    && now.getHours() == pro.timeEnd.getHours() 
                    && (Math.abs(pro.timeEnd.getMinutes() - now.getMinutes())) <= 5){
                    //tang 10'
                    console.log(Math.abs(pro.timeEnd.getMinutes() - now.getMinutes()));
                    pro.timeEnd.setMinutes(pro.timeEnd.getMinutes() + 10);
                }

                pro.save(function (err, pros) {
                    if (err) {
                        return res.json({
                            message: 'Error when getting pros',
                            error: err
                        });
                    }
                });
            }
            //return res.json({pro});

            userModule.findOne({_id: idUS}, function (err, us) {
            if (err) {
                return json({
                    message: 'Error when getting user',
                    error: err
                });
            }
            if (!us) {
                return json({
                    message: 'No such user'
                });
            }
           
            us.lstAucti.push(idPro);

            us.save(function (err, us) {
                
                if (err) {
                    return json({
                        message: 'Error when updating user.',
                        error: err
                    });
                }
            });

            return res.json({us});
        });
        });

        
    },

    Kich : function (req, res){
        var idUS = req.body.idUS; 
        var idPro = req.params.id;
        productModule.findOne({_id: idPro}, function (err, pro) {
            if (err) {
                return json({
                    message: 'Error when getting product',
                    error: err
                });
            }
            if (!pro) {
                return res.json({
                    message: 'No such '
                });
            }
            
            //push vào ds
            pro.lstIdUserKick.push(idUS);

            //cập nhật lại thông tin nếu kich us đang zữ zá max
<<<<<<< HEAD
<<<<<<< HEAD
            for(var i = pro.lstIdUserAuc.length-1; i >= 0; i--){
                if(pro.lstIdUserAuc[i].idUS != idUS){
=======
=======
=======
<<<<<<< HEAD
            for(var i = pro.lstIdUserAuc.length-1; i >= 0; i--){
                if(pro.lstIdUserAuc[i].idUS != idUS){
=======
>>>>>>> master
>>>>>>> deverlopment
            // for(var i = pro.lstIdUserAuc.length-2; i >= 0; i--){
            //     if(pro.lstIdUserAuc[i].idUS != idUS){
            //         pro.idUserMaxPrice = pro.lstIdUserAuc[i].idUS;
            //         pro.price = pro.lstIdUserAuc[i].price + pro.jump;
            //         break;
            //     }
            // }    
            for(var i = pro.lstIdUserAuc.length-2; i >= 0; i--){
                if(pro.lstIdUserKick.indexOf(pro.lstIdUserAuc[i].idUS) == -1){
>>>>>>> deverlopment
                    pro.idUserMaxPrice = pro.lstIdUserAuc[i].idUS;
                    pro.price = pro.lstIdUserAuc[i].price + pro.jump;
                    break;
                }
            }       
            

            if(pro.top2[0] && pro.top2[0].idUS == idUS){
                //xoa vt 0 va xoa 1 thang
                pro.top2.splice(0, 1);
            }

<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
<<<<<<< HEAD
>>>>>>> deverlopment
            pro.save(function (err, pro) {
                
                if (err) {
                    return  err;
                }
            });

            return res.json({pro});
=======
<<<<<<< HEAD
=======
>>>>>>> master
>>>>>>> deverlopment
            userModule.findOne({_id: idUS}, function (err, us) {
                if (err) {
                    return res.json({
                        message: 'Error when getting user',
                        error: err
                    });
                }
                nodemailer.createTestAccount((err, account) => {
                    
                    let transporter = nodemailer.createTransport({
                        host: 'smtp.gmail.com',
                        
                        auth: {
                            user: 'hdbao2009@gmail.com', // generated ethereal user
                            pass: '0918700845' // generated ethereal password
                        }
                    });

                    // setup email data with unicode symbols
                    let mailOptions = {
                        from: 'hdbao2009@gmail.com', // sender address
                        to: us.eMail, // list of receivers
                        subject: 'Đấu giá', // Subject line
                        text: 'Bạn đã bị Kich khỏi quá trình đấu zá', // plain text body
                        html: `<b>Đã bị KICH và không được quyền đấu zá sp này nửa </b>` // html body
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                    });
                });
            });


            pro.save(function (err, pro) {
                if (err) {
                    return  err;
                }
                return res.json({pro});
            });
            // return res.json({pro});
<<<<<<< HEAD
>>>>>>> deverlopment
=======
<<<<<<< HEAD
=======
>>>>>>> deverlopment
>>>>>>> master
>>>>>>> deverlopment
        });

    }

};

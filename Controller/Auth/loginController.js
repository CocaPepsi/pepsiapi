var userModel = require('../../Modules/userModule.js');
const bcrypt = require('bcrypt');
var emailCheck = require('email-check');
var jwt = require('jsonwebtoken');

var SECRET_KEY = 'secret';

 module.exports = {

    login: function (req, res) {
        var email = req.body.eMail;
        userModel.findOne({eMail: email}, function (err, user) {
            if (err) {
               return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            if(bcrypt.compareSync(req.body.pass, user.pass)){
                var payload = {
                    id: user._id,
                    email: email,
                    user: req.body.user,
                    info: 'for you'
                }
                var token = jwt.sign(payload, SECRET_KEY, {
                    expiresIn: 120
                });
                return res.status(201).json({
                    message: 'dang nhap thanh cong',
                    access_token: token
                });
            }
            else {
                return res.status(404).json({
                    message: 'tai khoan sai',
                    
                });
            }

            //return res.status(204).json();
            
        });
    }
}
var userModel = require('../../Modules/userModule.js');
var adminModule = require('../../Modules/adminModule.js');
const bcrypt = require('bcrypt');
var emailCheck = require('email-check');
var jwt = require('jsonwebtoken');

var SECRET_KEY = 'secret';

 module.exports = {

    login: function (req, res) {
        var eMailAdmin = req.body.eMailAdmin;
        adminModule.findOne({eMailAdmin: eMailAdmin}, function (err, ad) {
            if (err) {
               return res.status(500).json({
                    message: 'Error when deleting the ad.',
                    error: err
                });
            }
            if(bcrypt.compareSync(req.body.pass, ad.pass)){
                var payload = {
                    id: ad._id,
                    eMailAdmin: eMailAdmin,
                    admin: ad,
                    info: 'for you'
                }
                var token = jwt.sign(payload, SECRET_KEY, {
                    expiresIn: 120
                });
                return res.status(201).json({
                    message: 'dang nhap thanh cong',
                    access_token: token
                });
            }
            else {
                return res.status(404).json({
                    message: 'tai khoan sai',
                    
                });
            }

            //return res.status(204).json();
            
        });
    }
}
var assessModule = require('../Modules/assessModule.js');
var userModule = require('../Modules/userModule.js');

/**
 * assessController.js
 *
 * @description :: Server-side logic for managing asss.
 */
module.exports = {

    /**
     * assessController.list()
     */
    list: function (req, res) {
        assessModule.find(function (err, ass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ass.',
                    error: err
                });
            }
            return res.json(ass);
        });
    },

    /**
     * assessController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        assessModule.findOne({_id: id}, function (err, ass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ass.',
                    error: err
                });
            }
            if (!ass) {
                return res.status(404).json({
                    message: 'No such ass'
                });
            }
            return res.json(ass);
        });
    },

    /**
     * assessController.create()
     */
    create: function (req, res) {
        var ass = new assessModule({
            userID : req.body.userID,//ngừi bị đánh zá
            IdAssess : req.body.IdAssess,//ngừi đáng zá
            messenger : req.body.messenger,//lời nhắn
            score : req.body.score
        });

        ass.save(function (err, ass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating ass',
                    error: err
                });
            }
            
        });

        userModule.findOne({_id : ass.userID}, function(err, user){
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating user',
                    error: err
                });
            }

            user.lstAssess.push(ass._id);
            user.assess+=ass.score;

            user.save(function (err, ass) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when creating user',
                        error: err
                    });
                }
                
            });
        });
        return res.status(201).json(ass);
    },

    /**
     * assessController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        assessModule.findOne({_id: id}, function (err, ass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ass',
                    error: err
                });
            }
            if (!ass) {
                return res.status(404).json({
                    message: 'No such ass'
                });
            }

            ass.assName = req.body.assName ? req.body.assName : ass.assName;
			ass.lstProduct = req.body.lstProduct ? req.body.lstProduct : ass.lstProduct;
           
            ass.save(function (err, ass) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating ass.',
                        error: err
                    });
                }

                return res.json(ass);
            });
        });
    },

    /**
     * assessController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        assessModule.findByIdAndRemove(id, function (err, ass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the ass.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};

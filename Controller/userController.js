var userModel = require('../Modules/userModule.js');
var assessModule = require('../Modules/assessModule.js');
const bcrypt = require('bcrypt');
var emailCheck = require('email-check'),
    bodyParser = require('body-parser'),
    axios = require('axios');


var userRepo = require('../Repo/userRepo');
var userModule = require('../Modules/userModule.js');
var productModule = require('../Modules/productModule.js');
/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {

    /**
     * userController.list()
     */
    list: function (req, res) {
        userModel.find(function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            return res.json(user);
        });
    },

    /**
     * userController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        userModel.findOne({ _id: id }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }
            assessModule.find({ _id: { $in: user.lstAssess } }, function (err, ass) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when getting ass.',
                        error: err
                    });
                }

                return res.json({ user, ass });
            });

        });
    },

    /**
     * userController.create()
     */
    create: function (req, res) {
        console.log("co zo khong");
        var user = new userModel({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            address: req.body.address,
            assess: 0,
            pass: req.body.pass,
            eMail: req.body.eMail,
            requirement: false,
            catUser: false,
            dateGrant: null,
            dateExpired: null
        });

        userModel.findOne({ eMail: req.body.eMail }, function (err, us) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            if (us) {
                return res.json({
                    message: 'mail already exists.'
                });
<<<<<<< HEAD

            } else {


                emailCheck(user.eMail)
                    .then(function (resMail) {
                        if (resMail) {
                            //hash pass user
                            var hash = bcrypt.hashSync(user.pass, 8);
                            user.pass = hash;


                            //insert 
                            user.save(function (err, user) {
                                if (err) {
                                    return res.status(500).json({
                                        message: 'Error when creating user',
                                        error: err
                                    });
                                }
                                return res.status(201).json(user);
=======
<<<<<<< HEAD
            }else{

                emailCheck(user.eMail)
        .then(function (resMail) {
            if(resMail){
                //hash pass user
                var hash = bcrypt.hashSync(user.pass, 8);
                user.pass = hash;
          
                
                //insert 
                user.save(function (err, user) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when creating user',
=======
            }
<<<<<<< HEAD
=======
            emailCheck(user.eMail)
            .then(function (resMail) {
                if(resMail){
                    //hash pass user
                    var hash = bcrypt.hashSync(user.pass, 8);
                    user.pass = hash;
              
                    
                    //insert 
                    user.save(function (err, user) {
                        if (err) {
                            return res.status(500).json({
                                message: 'Error when creating user',
                                error: err
>>>>>>> master
                            });
                        }
                    })
                    .catch(function (err) {
                        return res.status(500).json({
                            message: "eMail don't exist",
<<<<<<< HEAD

                            error: err
                        });
                    });

            }
        });
        // console.log(user);
        // Quick version
=======
>>>>>>> master
                            error: err
                        });
            });
>>>>>>> deverlopment
        });
<<<<<<< HEAD
            }
        });
=======
>>>>>>> master
    // console.log(user);
        // Quick version
        emailCheck(user.eMail)
        .then(function (resMail) {
            if(resMail){
                //hash pass user
                var hash = bcrypt.hashSync(user.pass, 8);
                user.pass = hash;
          
                
                //insert 
                user.save(function (err, user) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when creating user',
                            error: err
                        });
                    }
                    return res.status(201).json(user);
                });
            }
        })
        .catch(function (err) {
            return res.status(500).json({
                        message: "eMail don't exist",
                        error: err
                    });
        });
<<<<<<< HEAD
=======
>>>>>>> master


>>>>>>> deverlopment

    },

    /**
     * userController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        userModel.findOne({ _id: id }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }

            user.firstName = req.body.firstName ? req.body.firstName : user.firstName;
            user.lastName = req.body.lastName ? req.body.lastName : user.lastName;
            user.address = req.body.address ? req.body.address : user.address;
            user.pass = req.body.pass ? req.body.pass : user.pass;
            //user.eMail = req.body.eMail ? req.body.eMail : user.eMail;
            // user.catUser = req.body.catUser ? req.body.catUser : user.catUser;


            // Quick version
            if (req.body.eMail != null) {
                user.eMail = req.body.eMail;
                emailCheck(user.eMail)
                    .then(function (resMail) {
                        if (resMail) {
                            //hash pass user                    
                            if (req.body.pass) {
                                user.pass = req.body.pass;
                                var hash = bcrypt.hashSync(user.pass, 8);
                                user.pass = hash;
                                /*
                               var res = bcrypt.compareSync('1234', user.pass)  ; // true
                               console.log('equal');
                               console.log(res);*/
                            }
                            else {
                                user.pass = user.pass;
                            }
                            //insert 
                            user.save(function (err, user) {
                                if (err) {
                                    return res.status(500).json({
                                        message: 'Error when creating user',
                                        error: err
                                    });
                                }
                                return res.status(201).json(user);
                            });
                        }
                    })
                    .catch(function (err) {
                        return res.status(500).json({
                            message: "eMail don't exist",
                            error: err
                        });
                    });
            }
            else {
                user.eMail = user.eMail;
            }


        });
    },

    /**
     * userController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        userModel.findByIdAndRemove(id, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    /**
     * userController.remove()
     */
    captcha: function (req, res) {
        var secret = '6Lff6VAUAAAAAOAPAgadheTQ19sUm0kJSeBXT353';
        var captcha_response = req.body.captcha_response;
        // console.log(req);
        var url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${captcha_response}`;
        axios.post(url, {}, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            }
        })
            .then(function (response) {
                // console.log(response.data);
                res.json(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    },

    secured: function (req, res) {
        res.json({
            msg: 'secured message',
            payload: req.tokenPayload
        });
    },

    //ds các pro đang còn hạng đấu zá cuat US
    ListProBeingAucByUs: function (req, res) {
        var now = new Date();
        var id = req.params.id;
        productModule.find({ idSeller: id, timeEnd: { $gte: now } }, function (err, pro) {
            if (err) {
                return json({
                    message: 'Error when getting product',
                    error: err
                });
            }
            return res.json({ pro });
        });

    },

    //ds các pro đã có ngừi mua
    ListProDeposit: function (req, res) {
        var now = new Date();
        var id = req.params.id;
<<<<<<< HEAD
        productModule.find({ idSeller: id, timeEnd: { $lt: now }, idUserMaxPrice: { $nin: [null] } }, function (err, pro) {

=======
        productModule.find({idSeller: id, timeEnd : {$lt : now}, idUserMaxPrice : {$nin : [null]}}, function (err, pro) {
            console.log("asdasd");
<<<<<<< HEAD
=======
>>>>>>> master
>>>>>>> deverlopment
            if (err) {
                return err;
            }

            return res.json({ pro });
        });

    },

    //ds các pro đã có ngừi mua
    viewWatchList: function (req, res) {
        var lstPro = [1];
        userModule.findOne({ _id: req.params.id }, function (err, user) {
            if (err) {
                return err;
            }

            productModule.find({ _id: { $in: user.watchList } }, function (err, pro) {
                if (err) {
                    return err;
                }

                return res.json({ pro });
            });
        });

    },

    //ds các pro đang đấu zá
    lstAucti: function (req, res) {
        userModule.findOne({ _id: req.params.id }, function (err, user) {
            if (err) {
                return err;
            }

            productModule.find({ _id: { $in: user.lstAucti } }, function (err, pro) {
                if (err) {
                    return err;
                }

                return res.json({ pro });
            });
        });

    },

    // danh sach cac san pham user đa đăng
    ListProAvaiByUs: function (req, res) {
        var now = new Date();
        var id = req.params.id;
        productModule.find({ idSeller: id }, function (err, pro) {
            if (err) {
                return json({
                    message: 'Error when getting product',
                    error: err
                });
            }
            return res.json({ pro });
        });

    },

<<<<<<< HEAD
=======
<<<<<<< HEAD
    // danh sach cac san pham user đa thắng
    lstAucWin: function (req, res) {
        userModule.findOne({ _id: req.params.id }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }

            productModule.find({ _id: { $in: user.lstAucWin } }, function (err, pro) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when deleting the user.',
                        error: err
                    });
                }

                return res.json({ pro });
            });
        });

    },

    // danh sach cac đánh zá
    /*lstAssess: function(req, res){
        userModule.findOne({_id : req.params.id}, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            
            assessModule.find({_id: {$in: user.lstAssess}}, function (err, ass) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when deleting the user.',
                        error: err
                    });
                }

                return res.json({ass});
            });
        });

    },*/

=======
>>>>>>> master
>>>>>>> deverlopment
    //thêm pro vào ds iu thích
    addWatchList: function (req, res) {
        var idUS = req.params.id;
        var idPro = req.body.idPro;
        userModule.findOne({ _id: idUS }, function (err, user) {
            if (err) {
                console.log(idUS);
                return err;
            }

            user.watchList.push(idPro);
            user.save(function (err, user) {
                if (err) {
                    return err;
                }
                return res.json({ user });
            });
        });
    },


    //thêm pro vào ds đã thắng
    addLstAucWin: function (req, res) {
        var idUS = req.params.id;
        var idPro = req.body.idPro;
<<<<<<< HEAD
        userRepo.addLstAucWin(idUS, idPro);
=======
<<<<<<< HEAD
        userModule.findOne({ _id: idUS }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }

            user.watchList.push(idPro);
            user.save(function (err, user) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when deleting the user.',
                        error: err
                    });
                }
                return res.json({ user });
            });
        });
=======
        userRepo.addLstAucWin(idUS, idPro);
>>>>>>> master
>>>>>>> deverlopment

    },

    Requirement: function (req, res) {
        var idUS = req.params.id;
        userModule.findOne({ _id: idUS }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            if (!user) {
                return 'No such user';
            }
            user.requirement = true;
            // Quick version
            user.save(function (err, user) {
                if (err) {
                    return err;
                }
<<<<<<< HEAD
                return res.json({ user });
=======
                if (!user) {
                    return 'No such user';
                }
                user.requirement = true;
                // Quick version
                user.save(function (err, user) {
                    if (err) {
                        return err;
                    }
                    return res.json({user});
                });
>>>>>>> master
            });
        });
    },

    UpdateCatUserByAdmin: function (req, res) {
        var idUS = req.params.id;
            userModule.findOne({_id: idUS}, function (err, user) {
                if (err) {
                    return err;
                }
                if (!user) {
                    return 'No such user';
                }
                user.catUser = true;
                user.dateGrant = 
                // Quick version
                user.save(function (err, user) {
                    if (err) {
                        return err;
                    }
                    return res.json({user});
                });
            });
    }
};
